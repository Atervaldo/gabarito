
package rodarte.testern.banco;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Student {

    public static final String SEX_M = "M";

    public static final String SEX_F = "F";

    public static final String LBL_IDENTIFICACAO = "Identificação";

    public static final String LBL_NOME = "Nome";

    public static final String LBL_SEXO = "Sexo";

    public static final String LBL_DATA_NASCIMENTO = "Data de Nascimento";

    public static final String LBL_IDADE = "Idade";

    private Integer id;

    private Integer code;

    private String name;

    private String sex;

    private Date birthDate;

    private final List<Grade> grades;

    public Student() {
        grades = new ArrayList<Grade>(0);
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate the birthDate to set
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return the grades
     */
    public List<Grade> getGrades() {
        return grades;
    }

    /**
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "Student [birthDate=" + birthDate + ", code=" + code + ", id=" + id + ", name=" + name + ", sex=" + sex + "]";
    }

    public Grade adicionarNota(int ano,
                               int trimestre,
                               double nota) {
        Grade grade = new Grade();
        grade.setGrade(nota);
        grade.setQuarter(trimestre);
        grade.setYear(ano);
        grade.setStudentId(id);
        grades.add(grade);

        return grade;
    }

    public Integer getIdade() {
        LocalDate now = LocalDate.now();
        LocalDate birthDateLd = Instant.ofEpochMilli(birthDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

        return Period.between(birthDateLd, now).getYears();
    }

    public double getMediaNotas() {
        return getSomaNotas() / grades.size();
    }

    public double getSomaNotas() {
        double soma = 0;

        for (Grade grade : grades) {
            soma += grade.getGrade();
        }

        return soma;
    }

    public static Comparator<Student> nameComparator() {
        return new Comparator<Student>() {

            @Override
            public int compare(Student o1,
                               Student o2) {
                return o1.getName().compareTo(o2.getName());
            }

        };
    }

    public static Comparator<Student> ageComparator() {
        return new Comparator<Student>() {

            @Override
            public int compare(Student o1,
                               Student o2) {
                return o1.getIdade().compareTo(o2.getIdade());
            }

        };

    }

}


package rodarte.testern.banco;

public class Grade implements Comparable<Grade> {

    private Integer id;

    private Integer studentId;

    private Integer year;

    private Integer quarter;

    private Double grade;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the studentId
     */
    public Integer getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * @return the quarter
     */
    public Integer getQuarter() {
        return quarter;
    }

    /**
     * @param quarter the quarter to set
     */
    public void setQuarter(Integer quarter) {
        this.quarter = quarter;
    }

    /**
     * @return the grade
     */
    public Double getGrade() {
        return grade;
    }

    /**
     * @param grade the grade to set
     */
    public void setGrade(Double grade) {
        this.grade = grade;
    }
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "Grade [grade=" + grade + ", id=" + id + ", quarter=" + quarter + ", studentId=" + studentId + ", year=" + year + "]";
    }

    @Override
    public int compareTo(Grade o) {
        int yearCmp = getYear().compareTo(o.getYear());

        if (yearCmp != 0) {
            return yearCmp;
        } else {
            return getQuarter().compareTo(o.getQuarter());
        }
    }

}

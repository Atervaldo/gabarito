
package rodarte.testern.viewmodel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;
import rodarte.testern.banco.Grade;
import rodarte.testern.banco.Student;
import rodarte.testern.service.StudentFileService;
import rodarte.testern.service.StudentService;
import rodarte.testern.service.StudentStatisticsModel;

public class VmRelatorio {

    private static final String DECIMAL_FORMAT = "0.00";

    private static final String PCT_FORMAT = "#,##0.00 %";

    private List<String> plan1Colunas;

    private List<String> plan2Colunas;

    private List<String> plan3Colunas;

    private List<List<String>> plan1Dados;

    private List<List<String>> plan2Dados;

    private List<List<String>> plan3Dados;

    private List<Student> estudantes;

    @Init
    public void init() {
        plan1Colunas = Arrays.asList(Student.LBL_IDENTIFICACAO, Student.LBL_NOME, Student.LBL_SEXO, Student.LBL_DATA_NASCIMENTO,
                "Nota 1º Trimestre", "Nota 2º Trimestre", "Nota 3º Trimestre");
        plan2Colunas = Arrays.asList(Student.LBL_IDENTIFICACAO, Student.LBL_NOME, Student.LBL_IDADE, "Média das Notas");
        plan3Colunas = Arrays.asList(StudentStatisticsModel.LBL_ESTATISTICAS);

        plan1Dados = new ArrayList<>(0);
        plan2Dados = new ArrayList<>(0);
        plan3Dados = new ArrayList<>(0);
    }

    @Command
    public void carregarResultados() {
        plan1Dados.clear();
        plan2Dados.clear();
        plan3Dados.clear();

        StudentService studentService = new StudentService();

        estudantes = studentService.listarTodos();

        StudentStatisticsModel estatisticas = new StudentStatisticsModel(StudentService.CRITERIO_APROVACAO, estudantes);

        processarPlan1Dados(estudantes);
        processarPlan2Dados(estudantes);
        processarPlan3Dados(estatisticas);

        BindUtils.postNotifyChange(null, null, this, "plan1Dados");
        BindUtils.postNotifyChange(null, null, this, "plan2Dados");
        BindUtils.postNotifyChange(null, null, this, "plan3Dados");
    }

    @Command
    public void exportarResultados() {
        if (estudantes == null || estudantes.isEmpty()) {
            Messagebox.show("Resultados não carregados! Clique no botão 'Carregar Resultados'", "Atenção", Messagebox.OK, Messagebox.ERROR);
            return;
        }

        try {
            File arquivo = new File("Resultados.xlsx").getCanonicalFile();
            OutputStream out = new FileOutputStream(arquivo.getAbsolutePath());

            StudentService ss = new StudentService();
            List<Student> students = ss.listarTodos();

            StudentFileService sfs = new StudentFileService();
            sfs.writeFile(out, students);

            Filedownload.save(arquivo, ".xlsx");

            Clients.showNotification("Arquivo exportado com sucesso!", Clients.NOTIFICATION_TYPE_INFO, null, null, 3500, true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Messagebox.show(ex.getMessage(), "Erro ao exportar Resultados", Messagebox.OK, Messagebox.ERROR);
        }

    }

    public List<String> getPlan1Colunas() {
        return plan1Colunas;
    }

    public List<String> getPlan2Colunas() {
        return plan2Colunas;
    }

    public List<String> getPlan3Colunas() {
        return plan3Colunas;
    }

    public List<List<String>> getPlan1Dados() {
        return plan1Dados;
    }

    public List<List<String>> getPlan2Dados() {
        return plan2Dados;
    }

    public List<List<String>> getPlan3Dados() {
        return plan3Dados;
    }

    private void processarPlan1Dados(List<Student> estudantes) {
        // Ordenando estudantes por nome
        Collections.sort(estudantes, Student.nameComparator());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);

        estudantes.forEach(estudante -> {
            List<String> linhaEstudante = new ArrayList<>();
            plan1Dados.add(linhaEstudante);

            linhaEstudante.add(estudante.getCode().toString());
            linhaEstudante.add(estudante.getName());
            linhaEstudante.add(estudante.getSex());
            linhaEstudante.add(sdf.format(estudante.getBirthDate()));

            // Notas
            // Ordenar por ano / trimestre
            List<Grade> notas = estudante.getGrades();
            Collections.sort(notas);

            for (Grade nota : notas) {
                linhaEstudante.add(df.format(nota.getGrade()));
            }
        });
    }

    private void processarPlan2Dados(List<Student> estudantes) {

        // Ordenando estudantes por idade
        Collections.sort(estudantes, Student.ageComparator());

        DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);

        estudantes.forEach(estudante -> plan2Dados.add(Arrays.asList(estudante.getCode().toString(), estudante.getName(),
                estudante.getIdade().toString(), df.format(estudante.getMediaNotas()))));
    }

    private void processarPlan3Dados(StudentStatisticsModel estatisticas) {
        DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
        DecimalFormat pf = new DecimalFormat(PCT_FORMAT);

        plan3Dados.add(
                Arrays.asList(StudentStatisticsModel.LBL_PCT_ALUNOS_SEXO_MASCULINO, pf.format(estatisticas.getPctAlunosSexoMasculino())));
        plan3Dados.add(
                Arrays.asList(StudentStatisticsModel.LBL_PCT_ALUNOS_SEXO_FEMININO, pf.format(estatisticas.getPctAlunosSexoFeminino())));
        plan3Dados
                .add(Arrays.asList(StudentStatisticsModel.LBL_PCT_ALUNOS_MENOS_30_ANOS, pf.format(estatisticas.getPctAlunosMenos30Anos())));
        plan3Dados.add(Arrays.asList(StudentStatisticsModel.LBL_PCT_ALUNOS_APROVADOS, pf.format(estatisticas.getPctAlunosAprovados())));
        plan3Dados.add(Arrays.asList(StudentStatisticsModel.LBL_MED_NOTA_ALUNOS_MAIS_30_ANOS,
                df.format(estatisticas.getMedNotaAlunosMais30Anos())));
        plan3Dados.add(Arrays.asList(StudentStatisticsModel.LBL_MED_NOTA_ALUNOS_SEXO_MASCULINO,
                df.format(estatisticas.getMedNotaAlunosSexoMasculino())));
        plan3Dados.add(Arrays.asList(StudentStatisticsModel.LBL_MED_NOTA_ALUNOS_SEXO_FEMININO,
                df.format(estatisticas.getMedNotaAlunosSexoFeminino())));
        plan3Dados.add(Arrays.asList(StudentStatisticsModel.LBL_MED_IDADE, Integer.toString((int) estatisticas.getMedIdade())));
    }

}

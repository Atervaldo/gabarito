
package rodarte.testern.service;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import rodarte.testern.banco.Student;

public class StudentStatisticsModel {

    public static final String LBL_ESTATISTICAS = "Estatísticas";

    public static final String LBL_PCT_ALUNOS_SEXO_MASCULINO = "Percentual de alunos do sexo masculino";

    public static final String LBL_PCT_ALUNOS_SEXO_FEMININO = "Percentual de alunos do sexo feminino";

    public static final String LBL_PCT_ALUNOS_MENOS_30_ANOS = "Percentual de alunos com menos de 30 anos";

    public static final String LBL_PCT_ALUNOS_APROVADOS = "Percentual de alunos aprovados";

    public static final String LBL_MED_NOTA_ALUNOS_MAIS_30_ANOS = "Média de nota dos alunos com mais de 30 anos";

    public static final String LBL_MED_NOTA_ALUNOS_SEXO_MASCULINO = "Média de nota dos alunos do sexo masculino";

    public static final String LBL_MED_NOTA_ALUNOS_SEXO_FEMININO = "Média de nota dos alunos do sexo feminino";

    public static final String LBL_MED_IDADE = "Média de idade dos participantes da base";

    private List<Student> estudantes;

    private double criterioAprovacao;

    private double idade30Anos;

    private double qtdAlunos;

    private double qtdAlunosMasculinos;

    private double qtdAlunosFemininos;

    private double qtdAlunosMenores30Anos;

    private double qtdAlunosAprovados;

    private OptionalDouble mediaNotaAlunosMaiores30Anos;

    private OptionalDouble mediaNotaAlunosMasculinos;

    private OptionalDouble mediaNotaAlunosFemininos;

    private OptionalDouble mediaIdade;

    public StudentStatisticsModel(double criterioAprovacao,
                                  List<Student> estudantes) {
        this.criterioAprovacao = criterioAprovacao;

        this.estudantes = new ArrayList<>(0);

        if (estudantes != null) {
            this.estudantes.addAll(estudantes);
        }

        // Criterio de aprovacao = 70%
        idade30Anos = 30;
        qtdAlunos = this.estudantes.size();
        qtdAlunosMasculinos = this.estudantes.stream().filter(s -> Student.SEX_M.equals(s.getSex())).count();
        qtdAlunosFemininos = this.estudantes.stream().filter(s -> Student.SEX_F.equals(s.getSex())).count();
        qtdAlunosMenores30Anos = this.estudantes.stream().filter(s -> s.getIdade().intValue() <= idade30Anos).count();
        qtdAlunosAprovados = this.estudantes.stream().filter(s -> s.getSomaNotas() >= this.criterioAprovacao).count();

        mediaNotaAlunosMaiores30Anos = this.estudantes.stream().filter(s -> s.getIdade().intValue() > idade30Anos)
                .mapToDouble(s -> s.getGrades().stream().mapToDouble(g -> g.getGrade()).sum()).average();

        mediaNotaAlunosMasculinos = this.estudantes.stream().filter(s -> Student.SEX_M.equals(s.getSex()))
                .mapToDouble(s -> s.getGrades().stream().mapToDouble(g -> g.getGrade()).sum()).average();

        mediaNotaAlunosFemininos = this.estudantes.stream().filter(s -> Student.SEX_F.equals(s.getSex()))
                .mapToDouble(s -> s.getGrades().stream().mapToDouble(g -> g.getGrade()).sum()).average();

        mediaIdade = estudantes.stream().mapToInt(s -> s.getIdade()).average();
    }

    public double getPctAlunosSexoMasculino() {
        return qtdAlunosMasculinos / qtdAlunos;
    }

    public double getPctAlunosSexoFeminino() {
        return qtdAlunosFemininos / qtdAlunos;
    }

    public double getPctAlunosMenos30Anos() {
        return qtdAlunosMenores30Anos / qtdAlunos;
    }

    public double getPctAlunosAprovados() {
        return qtdAlunosAprovados / qtdAlunos;
    }

    public double getMedNotaAlunosMais30Anos() {
        return mediaNotaAlunosMaiores30Anos.orElse(0);
    }

    public double getMedNotaAlunosSexoMasculino() {
        return mediaNotaAlunosMasculinos.orElse(0);
    }

    public double getMedNotaAlunosSexoFeminino() {
        return mediaNotaAlunosFemininos.orElse(0);
    }

    public double getMedIdade() {
        return mediaIdade.orElse(0);
    }

}

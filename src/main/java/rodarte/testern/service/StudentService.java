
package rodarte.testern.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rodarte.testern.banco.BancoConexao;
import rodarte.testern.banco.Grade;
import rodarte.testern.banco.Student;

public class StudentService {

    public static final double CRITERIO_APROVACAO = 70;

    public List<Student> listarTodos() {
        Map<Integer, Student> mapaEstudantes = new HashMap<>();

        Connection conn = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;

        try {
            conn = BancoConexao.abrir(BancoConexao.NOME_BANCO_DADOS_PRINCIPAL);
            if (conn != null) {
                StringBuilder sql = new StringBuilder();
                sql.append(" SELECT ").append("  s.id ").append(" ,s.code ").append(" ,s.fullName ").append(" ,s.sex ")
                        .append(" ,s.birthDay ").append(" ,g.year ").append(" ,g.quarter ").append(" ,g.grade ").append(" FROM students s ")
                        .append(" LEFT JOIN grades g ON g.studentId = s.id ").append(" ORDER BY s.fullName ");

                ps1 = conn.prepareStatement(sql.toString());
                rs1 = ps1.executeQuery();

                // Criar um mapa de code => student
                //

                while (rs1.next()) {
                    Integer studentCode = rs1.getInt(2);

                    Student s = mapaEstudantes.get(studentCode);

                    if (s == null) {
                        s = new Student();
                        s.setId(rs1.getInt(1));
                        s.setCode(studentCode);
                        s.setName(rs1.getString(3));
                        s.setSex(rs1.getString(4));
                        s.setBirthDate(rs1.getDate(5));
                        mapaEstudantes.put(s.getCode(), s);
                    }

                    int gradeYear = rs1.getInt(6);
                    int gradeQuarter = rs1.getInt(7);
                    double gradeGrade = rs1.getDouble(8);

                    s.adicionarNota(gradeYear, gradeQuarter, gradeGrade);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                BancoConexao.fechar(conn, ps1, null);
            } catch (Exception e) {
                // Ignorado!
            }
        }

        return new ArrayList<Student>(mapaEstudantes.values());
    }

    public List<Student> gravarNoBanco(List<Student> arg0) {
        System.out.println("Gravando " + arg0.size() + " Estudantes no banco de dados");

        Connection conn = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            conn = BancoConexao.abrir(BancoConexao.NOME_BANCO_DADOS_PRINCIPAL);

            if (conn != null) {
                ps1 = conn.prepareStatement(" INSERT INTO students (code, fullName, sex, birthDay) VALUES (?, ?, ?, ?) ");
                ps2 = conn.prepareStatement(
                        " INSERT INTO grades (studentId, year, quarter, grade) VALUES ( (SELECT id FROM students WHERE (code = ?)),  ?, ?, ?) ");
            }

            int[] ps1Results = null;

            if (ps1 != null) {
                for (Student student : arg0) {
                    ps1.setInt(1, student.getCode());
                    ps1.setString(2, student.getName());
                    ps1.setString(3, student.getSex());
                    ps1.setDate(4, new java.sql.Date(student.getBirthDate().getTime()));
                    ps1.addBatch();
                }

                // INSERT INTO grades (studentId, year, quarter, grade) VALUES ( SELECT id FROM
                // students WHERE (code = ?), ?, ?, ?)

                ps1Results = ps1.executeBatch();
            }

            if (ps2 != null && ps1Results != null) {
                for (Student student : arg0) {
                    for (Grade grade : student.getGrades()) {
                        ps2.setInt(1, student.getCode());
                        ps2.setInt(2, grade.getYear());
                        ps2.setInt(3, grade.getQuarter());
                        ps2.setDouble(4, grade.getGrade());
                        ps2.addBatch();
                    }
                }

                ps2.executeBatch();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                BancoConexao.fechar(conn, ps1, null);
                BancoConexao.fechar(conn, ps2, null);
            } catch (Exception e) {
                // Ignorado!
            }
        }

        System.out.println(arg0.size() + " Estudantes gravados no banco de dados");

        return listarTodos();
    }

    public void limparTabelas() {
        Connection conn = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            conn = BancoConexao.abrir(BancoConexao.NOME_BANCO_DADOS_PRINCIPAL);

            if (conn != null) {
                ps1 = conn.prepareStatement(" DELETE FROM grades ");
                ps2 = conn.prepareStatement(" DELETE FROM students ");
            }

            ps1.executeUpdate();
            ps2.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                BancoConexao.fechar(conn, ps1, null);
                BancoConexao.fechar(conn, ps2, null);
            } catch (Exception e) {
                // Ignorado!
            }
        }
    }

}
